import React from 'react';
import { Text, View, Button, AppRegistry } from 'react-native';

const gerarNumeroAleatorio = () => {
  let numeroAleatorio = Math.random();
  numeroAleatorio = Math.floor(numeroAleatorio * 10);
  alert(numeroAleatorio);
}

const App = () => {
  return (
    <View>
      <Text>Gerador de números randômicos</Text>
      <Button
        title="Gerar um número randômico"
        onPress={gerarNumeroAleatorio}
      />
    </View>
  )
}
AppRegistry.registerComponent('app1', () => App)